from unittest import skipIf
from unittest.mock import patch, Mock

from django.test import TestCase

from apps.weather.models import Weather, Location
from apps.weather.services import (
    look_up_weather,
    get_weather,
    search_geographic_position_by_address,
    get_geographic_position
)
from config.settings import SKIP_REAL


class WeatherTest(TestCase):

    def setUp(self):
        self.location = Location(
            address="Londrina",
            latitude=-23.3159066,
            longitude=-51.121633
        )

    @patch('apps.weather.services.requests.get')
    def test_request_response(self, mock_get):
        mock_get.return_value.ok = True
        response = look_up_weather(self.location)
        self.assertIsNotNone(response)

    @patch('apps.weather.services.requests.get')
    def test_look_up_weather_when_response_is_ok(self, mock_get):
        data_results = {
            "currently": {
                "temperature": 58.33,
            },
            "flags": {
                "units": "us"
            }
        }

        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = data_results
        response = look_up_weather(self.location)
        self.assertDictEqual(response.json(), data_results)

    @patch('apps.weather.services.requests.get')
    def test_look_up_when_response_is_not_ok(self, mock_get):
        mock_get.return_value.ok = False
        response = look_up_weather(self.location)
        self.assertIsNone(response)

    @patch('apps.weather.services.look_up_weather')
    def test_get_weather(self, mock_get_weather):
        data_results = {
            "currently": {
                "temperature": 58.33,
            },
            "flags": {
                "units": "us"
            }
        }
        mock_get_weather.return_value = Mock()
        mock_get_weather.return_value.json.return_value = data_results

        response = get_weather(self.location)
        self.assertTrue(mock_get_weather.called)

        weather = Weather(
            temperature=58.33,
            units='us'
        )

        self.assertEqual(response.temperature, weather.temperature)
        self.assertEqual(response.units, weather.units)
        self.assertEqual(response.formatted_temperature, f"{round(weather.temperature)}°F")

    @skipIf(SKIP_REAL, 'Skipping tests that hit the real API server.')
    def test_integration_contract(self):
        actual = look_up_weather(self.location)
        actual_keys = list(k for k, v in actual.json().items())

        with patch('apps.weather.services.requests.get') as mock_get:
            mock_get.return_value.ok = True
            mock_get.return_value.json.return_value = {
                "currently": {
                    "temperature": 58.33,
                },
                "flags": {
                    "units": "us"
                }
            }

            mocked = look_up_weather(self.location)
            mock_keys = mocked.json().keys()
        for mk in mock_keys:
            self.assertTrue(mk in actual_keys)


class GeographicLocationTest(TestCase):

    def setUp(self):
        pass

    @patch('apps.weather.services.requests.get')
    def test_request_response(self, mock_get):
        mock_get.return_value.ok = True
        address = '86036-590'
        response = search_geographic_position_by_address(address)
        self.assertIsNotNone(response)

    @patch('apps.weather.services.requests.get')
    def test_search_when_response_is_ok(self, mock_get):
        search_results = {
            "results": [
                {
                    "address_components": [
                        {
                            "long_name": "Londrina",
                            "short_name": "Londrina",
                            "types": [
                                "administrative_area_level_2",
                                "political"
                            ]
                        }
                    ],
                    "formatted_address": "Chácaras Gralha Azul, Londrina - PR, 86036-590, Brasil",
                    "geometry": {
                        "location": {
                            "lat": -23.3159066,
                            "lng": -51.121633
                        }
                    },
                }
            ],
            "status": "OK"
        }

        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = search_results

        address = '86036-590'
        response = search_geographic_position_by_address(address)
        self.assertDictEqual(response.json(), search_results)

    @patch('apps.weather.services.requests.get')
    def test_search_when_response_is_not_ok(self, mock_get):
        mock_get.return_value.ok = False
        address = '86036-590'
        response = search_geographic_position_by_address(search_query=address)
        self.assertIsNone(response)

    @patch('apps.weather.services.search_geographic_position_by_address')
    def test_get_geographic_position(self, mock_get_position):
        search_results = {
            "results": [
                {
                    "address_components": [
                        {
                            "long_name": "Londrina",
                            "short_name": "Londrina",
                            "types": [
                                "administrative_area_level_2",
                                "political"
                            ]
                        }
                    ],
                    "formatted_address": "Chácaras Gralha Azul, Londrina - PR, 86036-590, Brasil",
                    "geometry": {
                        "location": {
                            "lat": -23.3159066,
                            "lng": -51.121633
                        }
                    },
                }
            ],
            "status": "OK"
        }

        mock_get_position.return_value = Mock()
        mock_get_position.return_value.json.return_value = search_results

        address = '86036-590'
        response = get_geographic_position(address)

        self.assertTrue(mock_get_position.called)

        position = Location(
            address="Londrina",
            latitude=-23.3159066,
            longitude=-51.121633
        )

        self.assertEqual(response.address, position.address)
        self.assertEqual(response.latitude, position.latitude)
        self.assertEqual(response.longitude, position.longitude)

    @skipIf(SKIP_REAL, 'Skipping tests that hit the real API server.')
    def test_integration_contract(self):
        address = '86036-590'
        actual = search_geographic_position_by_address(address)
        actual_keys = actual.json().keys()

        with patch('apps.weather.services.requests.get') as mock_get:
            mock_get.return_value.ok = True
            mock_get.return_value.json.return_value = {
                "results": [
                    {
                        "address_components": [
                            {
                                "long_name": "Londrina",
                                "short_name": "Londrina",
                                "types": [
                                    "administrative_area_level_2",
                                    "political"
                                ]
                            }
                        ],
                        "formatted_address": "Chácaras Gralha Azul, Londrina - PR, 86036-590, Brasil",
                        "geometry": {
                            "location": {
                                "lat": -23.3159066,
                                "lng": -51.121633
                            }
                        },
                    }
                ],
                "status": "OK"
            }

            mocked = search_geographic_position_by_address(address)
            mocked_keys = mocked.json().keys()
        self.assertListEqual(list(actual_keys), list(mocked_keys))
