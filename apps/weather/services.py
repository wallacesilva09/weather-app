import requests

from django.utils.translation import ugettext_lazy as _

from .models import Weather, Location
from config.exceptions import APIProviderError
from config.settings import DARK_SKY_API_KEY, WEATHER_URL, GOOGLE_API_KEY, GEOGRAPHIC_POSITION_URL


def look_up_weather(location: Location) -> requests.Response:
    """
    Service for looking up the Weather using python-requests
    :param location:
    :return: requests.Response
    """
    api_url = f"{WEATHER_URL}{DARK_SKY_API_KEY}/{location.latitude},{location.longitude}"
    payload = {'units': 'auto'}
    try:
        response = requests.get(api_url, params=payload)
    except requests.exceptions.ConnectionError as e:
        raise APIProviderError(_("Unable to connect to Google API."), e)
    return response if response.ok else None


def get_weather(location: Location) -> Weather or None:
    """
    Service for getting the weather for a specif location
    :param location:
    :return: Weather
    """
    response = look_up_weather(location)
    if response is None:
        return None

    result = response.json()
    return Weather(
        temperature=result['currently']['temperature'],
        units=result['flags']['units']
    )


def search_geographic_position_by_address(search_query: str) -> requests.Response:
    """
    Service for searching a location given a search query
    :param search_query: str
    :return: requests.Response
    """
    payload = {'address': search_query, 'key': GOOGLE_API_KEY}
    try:
        response = requests.get(GEOGRAPHIC_POSITION_URL, params=payload)
    except requests.exceptions.ConnectionError as e:
        raise APIProviderError(_("Unable to connect to Google API."), e)
    return response if response.ok else None


def get_geographic_position(search_query: str) -> Location or None:
    """
    Service for getting a location after consuming the previous service
    :param search_query:
    :return:
    """
    response = search_geographic_position_by_address(search_query)
    if response is None:
        return None

    results = response.json()

    if results['status'] != "OK":
        return None

    for result in results['results']:
        formatted_address = result['formatted_address']
        for component in result['address_components']:
            if 'administrative_area_level_2' in component['types']:
                formatted_address = component['long_name']
                break
        return Location(
            search_query=search_query,
            address=formatted_address,
            latitude=result['geometry']['location']['lat'],
            longitude=result['geometry']['location']['lng']
        )


def get_client_ip(request) -> str:
    """
    Method to get client public IP address
    :param request: request
    :return: str
    """
    ip = request.META.get('REMOTE_ADDR')
    if not ip:
        x_forwarded_for = request.META.get('HTTP_CF_CONNECTING_IP')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
    return ip
