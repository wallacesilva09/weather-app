from datetime import timedelta, datetime

from django.db.models.signals import pre_save
from django.dispatch import receiver

from apps.weather.models import Weather
from apps.weather.services import get_geographic_position, get_weather


@receiver(pre_save, sender=Weather)
def save_weather(sender, instance: Weather, **kwargs) -> None:
    """
    Signal for getting location and temperature when saving a Weather
    :param sender: Weather
    :param instance: Weather
    :param kwargs:
    :return: None
    """
    if not instance.latitude or not instance.longitude:
        location = get_geographic_position(instance.location)

        if not instance.temperature and location is not None:
            weather = Weather.objects.filter(
                latitude=round(location.latitude, 3),
                longitude=round(location.longitude, 3),
                created_at__gte=datetime.now() - timedelta(minutes=60)
            ).first()

            if not weather:
                weather = get_weather(location)

            if weather:
                instance.latitude = round(location.latitude, 3)
                instance.longitude = round(location.longitude, 3)
                instance.temperature = weather.temperature
                instance.units = weather.units
                instance.city = location.address

