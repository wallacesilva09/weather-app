from django.core.exceptions import ValidationError
from django.shortcuts import render

from apps.weather.forms.weather_form import WeatherForm
from apps.weather.services import get_client_ip


def weather_view(request):
    form = WeatherForm(request.POST or None, initial={'ip_address': get_client_ip(request)})
    context = {'form': form}
    if form.is_valid():
        context.update({'weather': form.instance})
        form.save()
        form.clean()

    return render(request, "weather_form.html", context=context)
