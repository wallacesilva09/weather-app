from django.db import models
from django.utils.translation import ugettext_lazy as _


class Location(models.Model):
    search_query = models.CharField(_('Search Query'), max_length=300, null=False, blank=False)
    address = models.CharField(_('Address'), max_length=300, null=False, blank=False)
    latitude = models.DecimalField(_('Latitude'), max_digits=9, decimal_places=7, null=True)
    longitude = models.DecimalField(_('Latitude'), max_digits=10, decimal_places=7, null=True)

    class Meta:
        abstract = True


class Weather(models.Model):
    location = models.CharField(_('Location'), max_length=300, null=False, blank=False)
    city = models.CharField(_('City'), max_length=300, null=True, blank=True)
    latitude = models.DecimalField(_('Latitude'), max_digits=9, decimal_places=3, null=True)
    longitude = models.DecimalField(_('Latitude'), max_digits=10, decimal_places=3, null=True)
    temperature = models.DecimalField(_('Temperature'), max_digits=5, decimal_places=2, null=True)
    units = models.CharField(_('Units'), max_length=3, null=True, blank=True)
    ip_address = models.GenericIPAddressField(_('IP Address'), null=False, blank=False)

    created_at = models.DateTimeField(_('Created Date'), auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(_('Updated Date'), auto_now=True, null=True, blank=False)

    def __str__(self):
        return f"{self.city}:{self.formatted_temperature}"

    class Meta:
        app_label = 'weather'
        db_table = 'weathers'
        ordering = ('created_at',)
        verbose_name = _('Weather')
        verbose_name_plural = _('Weathers')

    @property
    def formatted_temperature(self):
        conversion_data = {
            "si": "C",
            "us": "F",
            "uk2": "C",
            "ca": "C",
        }
        converted = conversion_data.get(self.units, "C")
        return f"{round(self.temperature)}°{converted}"
