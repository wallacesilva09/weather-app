from django.apps import AppConfig


class WeatherConfig(AppConfig):
    name = 'apps.weather'

    def ready(self):
        from apps.weather import signals
