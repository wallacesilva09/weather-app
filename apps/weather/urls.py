from django.conf.urls import url

from .views import weather_view

app_name = 'weather'

urlpatterns = [
    url(r'^', weather_view, name='weather_view'),
]
