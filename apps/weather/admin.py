from django.contrib import admin

from .models import Weather


class WeatherAdmin(admin.ModelAdmin):
    list_display = ('city', 'formatted_temperature', 'ip_address', 'created_at')
    readonly_fields = (
        'location',
        'city',
        'latitude',
        'longitude',
        'temperature',
        'units',
        'ip_address',
        'created_at',
        'updated_at',
    )


admin.site.register(Weather, WeatherAdmin)